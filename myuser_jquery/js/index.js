
$(document).ready(function () {

    $("#btnEdicao").click(function () {
        mostrarEdicao();
    });


    $("#iptCPF").inputmask("999.999.999-99");

    $("#iptTelefone").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999",],
        keepStatic: true
    });

    $("#iptEmail").inputmask("email");

    $("#btnSalvar").click(function () {
        salvar();
    })
    $("#btnFiltro").click(function () {
        filtrarUsuarios();
    })

    consultarUsuarios();
});
var dadosConsulta;

function mostrarEdicao() {
    if ($("#frmEdicao").css('display') == "none") {
        $("#frmEdicao").show();
    } else {
        $("#frmEdicao").hide();
    }

}


function consultarUsuarios() {

    jQuery.ajax({
        type: 'GET',
        dataType: 'json',
        url: "http://localhost:8080/api/usuarios",
        beforeSend: function (x) {
            $("#tblusuarios").hide();
            $("#load").show();
        },
        success: function (data) {
            dadosConsulta = data;

            $(".dados").remove();
            for (i = 0; i < data.length; i++) {
                var tr = "<tr class=\"dados\" ><td>" + data[i].email + "</td><td>" + data[i].nome + "</td><td>" + data[i].perfil + "</td><td><a href=\"#\" onclick=\"habilitar(" + data[i].id + ")\" > " + data[i].habilitado + " </a> </td> <td> <a href=\"#\" onclick=\"editar(" + data[i].id + ")\" >Editar</a>  <a href=\"#\" onclick=\"excluirConfirma(" + data[i].id + ")\" >Excluir</a>  </td>  </tr>";
                $('#listUsuarios tr:last').after(tr);
            }
            $("#load").hide();
            $("#tblusuarios").show();
        }
    });

}


function filtrarUsuarios() {
    
    var vdata = {
        nome : $("#fltNome").val(),
        status: $("#slcFiltroSituacao option:selected").val(),
        perfil: $("#slcFiltroPerfil option:selected").val()
    };

    jQuery.ajax({
        type: 'GET',
        dataType: 'json',
        data: vdata,
        url: "http://localhost:8080/api/usuariofiltro",
        beforeSend: function (x) {
            $("#tblusuarios").hide();
            $("#load").show();
        },
        success: function (data) {
            dadosConsulta = data;

            $(".dados").remove();
            for (i = 0; i < data.length; i++) {
                var tr = "<tr class=\"dados\" ><td>" + data[i].email + "</td><td>" + data[i].nome + "</td><td>" + data[i].perfil + "</td><td><a href=\"#\" onclick=\"habilitar(" + data[i].id + ")\" > " + data[i].habilitado + " </a> </td> <td> <a href=\"#\" onclick=\"editar(" + data[i].id + ")\" >Editar</a>  <a href=\"#\" onclick=\"excluirConfirma(" + data[i].id + ")\" >Excluir</a>  </td>  </tr>";
                $('#listUsuarios tr:last').after(tr);
            }
            $("#load").hide();
            $("#tblusuarios").show();
        }
    });

}

function habilitar(var_id){
    var valorNoArray = jQuery.map(dadosConsulta, function (obj) {
        if (obj.id === var_id)
            return obj;
    });

    if (valorNoArray[0].habilitado == true){

        var vdata = {
            id: valorNoArray[0].id,
            email: valorNoArray[0].email,
            nome: valorNoArray[0].nome,
            cpf: valorNoArray[0].cpf,
            telefone: valorNoArray[0].telefone,
            funcao: valorNoArray[0].funcao,
            perfil: valorNoArray[0].perfil,
            habilitado: false
        };
        jQuery.ajax({
            contentType: 'application/json;charset=utf-8',
            type: 'PUT',
            data: JSON.stringify(vdata),
            url: "http://localhost:8080/api/usuario",
            beforeSend: function (x) {
                $("#tblusuarios").hide();
                $("#load").show();
            },
            success: function (data) {
                consultarUsuarios();
                msg("Usuário desabilitado com sucesso!");
            }
        });
    }else{
        var vdata = {
            id: valorNoArray[0].id,
            email: valorNoArray[0].email,
            nome: valorNoArray[0].nome,
            cpf: valorNoArray[0].cpf,
            telefone: valorNoArray[0].telefone,
            funcao: valorNoArray[0].funcao,
            perfil: valorNoArray[0].perfil,
            habilitado: true
        };
        jQuery.ajax({
            contentType: 'application/json;charset=utf-8',
            type: 'PUT',
            data: JSON.stringify(vdata),
            url: "http://localhost:8080/api/usuario",
            beforeSend: function (x) {
                $("#tblusuarios").hide();
                $("#load").show();
            },
            success: function (data) {
                consultarUsuarios();
                msg("Usuário habilitado com sucesso!");
            }
        });

    }

}

function editar(var_id) {
    var valorNoArray = jQuery.map(dadosConsulta, function (obj) {
        if (obj.id === var_id)
            return obj;
    });

    $("#frmEdicao").show();

    $("#iptid").val(var_id);
    $("#iptEmail").val(valorNoArray[0].email);
    $("#iptNome").val(valorNoArray[0].nome);
    $("#iptCPF").val(valorNoArray[0].cpf);
    $("#iptTelefone").val(valorNoArray[0].telefone);
    $("#slcFuncao").val(valorNoArray[0].funcao).change();
    $("#slcPerfil").val(valorNoArray[0].perfil).change();
    $("#chkHabilitado")[0].checked = valorNoArray[0].habilitado;


}

function excluirConfirma(var_id) {

    var valorNoArray = jQuery.map(dadosConsulta, function (obj) {
        if (obj.id === var_id)
            return obj;
    });

    valor = "Deseja realmente excluir o usuário " + valorNoArray[0].nome + " ?";

    var texto = "<div class=\"alert  alert-primary  alert-dismissible fade show\" role=\"alert\">";
    texto += valor;
    texto += "<button type=\"button\"  onclick=\"excluir(" + var_id + ")\">Sim</button>"
    texto += "<button type=\"button\"  data-dismiss=\"alert\" aria-label=\"Close\">";
    texto += "Não";
    texto += "</button>";
    texto += "</div>";

    $("#div_msg").html(texto);
}


function excluir(var_id) {




    var vdata = {
        id: var_id

    };

    jQuery.ajax({
        contentType: 'application/json;charset=utf-8',
        type: 'DELETE',
        data: JSON.stringify(vdata),
        url: "http://localhost:8080/api/usuario",
        beforeSend: function (x) {
            $("#tblusuarios").hide();
            $("#load").show();
        },
        success: function (data) {
            consultarUsuarios();
            msg("Exclusão efetuada com sucesso", "sucesso");
        }
    });

}

function salvar() {
    if (validaCPF() == false) {
        msg("Operação não realizada. CPF digitado é inválido.", "erro");

    } else {


        if ($("#iptid").val() == "0") {
            var vdata = {

                email: $("#iptEmail").val(),
                nome: $("#iptNome").val(),
                cpf: $("#iptCPF").val(),
                telefone: $("#iptTelefone").val(),
                funcao: $("#slcFuncao option:selected").val(),
                perfil: $("#slcPerfil option:selected").val(),
                habilitado: $("#chkHabilitado").is(':checked')
            };
            jQuery.ajax({
                contentType: 'application/json;charset=utf-8',
                type: 'POST',
                data: JSON.stringify(vdata),
                url: "http://localhost:8080/api/usuario",
                beforeSend: function (x) {
                    $("#tblusuarios").hide();
                    $("#load").show();
                },
                success: function (data) {
                    if (data.includes("msg")) {
                        consultarUsuarios();
                        msg("Cadastro efetuado com sucesso!", "sucesso");
                    } else {
                        msg("Operação não realizada. Usuário já incluído.");
                    }
                }
            });
        } else {
            var vdata = {
                id: $("#iptid").val(),
                email: $("#iptEmail").val(),
                nome: $("#iptNome").val(),
                cpf: $("#iptCPF").val(),
                telefone: $("#iptTelefone").val(),
                funcao: $("#slcFuncao option:selected").val(),
                perfil: $("#slcPerfil option:selected").val(),
                habilitado: $("#chkHabilitado").is(':checked')
            };

            jQuery.ajax({
                contentType: 'application/json;charset=utf-8',
                type: 'PUT',
                data: JSON.stringify(vdata),
                url: "http://localhost:8080/api/usuario",
                beforeSend: function (x) {
                    $("#tblusuarios").hide();
                    $("#load").show();
                },
                success: function (data) {
                    consultarUsuarios();
                    msg("Alteração efetuada com sucesso!", "sucesso");
                }
            });

        }
        $("#iptid").val(0);
        $("#iptEmail").val(""),
            $("#iptNome").val(""),
            $("#iptCPF").val(""),
            $("#iptTelefone").val(""),
            $("#slcFuncao").val("").change();
        $("#slcPerfil").val("").change();
        $("#chkHabilitado")[0].checked = true;
    }

}

function msg(valor, tipo) {
    var tipoErro = "";

    if (tipo == "erro") {
        tipoErro = "alert-danger";
    } else {
        tipoErro = "alert-success";
    }

    var texto = "<div class=\"alert " + tipoErro + " alert-dismissible fade show\" role=\"alert\">";
    texto += valor;
    texto += "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">";
    texto += "<span aria-hidden=\"true\">&times;</span>";
    texto += "</button>";
    texto += "</div>";

    $("#div_msg").html(texto);
}

function validaCPF() {
    value = jQuery.trim($("#iptCPF").val());
    value = value.replace('.', '');
    value = value.replace('.', '');
    cpf = value.replace('-', '');
    while (cpf.length < 11) cpf = "0" + cpf;
    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    var a = [];
    var b = new Number;
    var c = 11;
    for (i = 0; i < 11; i++) {
        a[i] = cpf.charAt(i);
        if (i < 9) b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x }
    b = 0;
    c = 11;
    for (y = 0; y < 10; y++) b += (a[y] * c--);
    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }

    var retorno = true;
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;

    return retorno;
}