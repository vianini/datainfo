package com.datainfo.myusers.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.datainfo.myusers.model.Usuario;

public interface  UsuarioRepository extends JpaRepository<Usuario, Long> {
	
		Usuario findById(long id);
		
		@Query("SELECT  u FROM Usuario u WHERE LOWER(u.nome) = LOWER(?1) or LOWER(u.email) = LOWER(?2) or LOWER(u.cpf) = LOWER(?3)")
		Usuario findByNameEmailCpf( String nome,  String email, String cpf);
		
		@Query("SELECT  u FROM Usuario u WHERE u.nome like ?1 or u.perfil like ?2 ")
		List<Usuario> findByNamePerfil( String nome,  String perfil);
		
		@Query("SELECT  u FROM Usuario u WHERE u.nome like ?1 or u.perfil like ?2 or habilitado is ?3 ")
		List<Usuario> findByNamePerfilSituacao( String nome,  String perfil, boolean situacao);
		
}
