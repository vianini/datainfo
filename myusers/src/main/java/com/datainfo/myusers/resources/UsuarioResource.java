package com.datainfo.myusers.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datainfo.myusers.model.Usuario;
import com.datainfo.myusers.repository.UsuarioRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api")
@Api(value = "API REST Usuarios")
public class UsuarioResource {

	@Autowired
	UsuarioRepository usuarioRepository;

	@ApiOperation(value = "Retorna uma lista de Usuarios")
	@GetMapping("/usuarios")
	public List<Usuario> listaUsuarios() {
		return usuarioRepository.findAll();
	}

	@ApiOperation(value = "Retorna um Usuario filtrado")
	@GetMapping("/usuariofiltro")
	public List<Usuario> listaUsuarioUnico(@RequestParam("nome") String nome,
			@RequestParam("perfil") String perfil,
			@RequestParam("status") String status) {
		if(nome.equals("")) {
			nome = "%";
		}else {
			nome = "%" + nome + "%";
		}
		if(perfil.equals("")) {
			perfil = "%";
		}
		
		List<Usuario> usu;
		if(status.equals("todos")) {
			 usu = usuarioRepository.findByNamePerfil(nome, perfil);
		}else {
			if(status.equals("1")) {
				usu = usuarioRepository.findByNamePerfilSituacao(nome, perfil,true);
			}else {
				usu = usuarioRepository.findByNamePerfilSituacao(nome, perfil,false);
			}
		}
		
		
		
		return usu;
	}

	@ApiOperation(value="Salva um Usuario")
	@PostMapping("/usuario")
	public String salvausuario(@RequestBody @Valid Usuario usuario) {
		
		Usuario teste =  usuarioRepository.findByNameEmailCpf(usuario.getNome(),usuario.getEmail(), usuario.getEmail());
		System.out.println(teste);
		if(teste != null) {
			return "erro:Operação não realizada. Usuário já incluído.";
		}else {
			usuarioRepository.save(usuario); 	
			return "msg:Cadastro efetuado com sucesso!";
		}	
	}

	@ApiOperation(value = "Deleta um usuario")
	@DeleteMapping("/usuario")
	public void deletausuario(@RequestBody @Valid Usuario usuario) {
		usuarioRepository.delete(usuario);
	}

	@ApiOperation(value = "Atualiza um usuario")
	@PutMapping("/usuario")
	public Usuario atualizausuario(@RequestBody @Valid Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
}
