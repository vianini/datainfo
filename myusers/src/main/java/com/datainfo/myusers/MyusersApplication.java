package com.datainfo.myusers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyusersApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyusersApplication.class, args);
	}

}
