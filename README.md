---

## Teste para DataInfo

Neste projeto está dividido em dois diretórios :
	myusers = Contem o backend
	myuser_jquery = Contém o frontend API

O backend foi desenvolvido com os seguintes frameworks.
	-- JPA - camada de banco de dados 
	-- Spring MVC - para gestão dos requests

Para banco de dados foi utilizado o Postgres 

O FrontEnd foi desenvolvido com os seguintes frameworks.
	--Jquery 
	--Bootrap

## Passos para executar o backend

Esta configurado na porta 8080
java -jar /myusers/target/myusers-0.0.1-SNAPSHOT.jar

## Passos para executar o frontend

Executar diretamente o index 
/myusers_jquery/index.html

Quais bibliotecas além das disponíveis pelo servidor você usou e porquê?
R:Spring MVC -- por ser simples instalação e contém integração com jpa entre outras ferramentas 

 
Qual o tempo aproximado gasto por você no desenvolvimento do software? (Não
deve ser considerado o tempo gasto para montagem do ambiente)

R:12 horas

Quais as dificuldades que você enfrentou no desafio?

Perdi muito tento tentando entender o Angular 7.0, apesar de ser interessante é complexo o desenvolvimento principalmente como criar um padrao para os componentes se comunicarem. Decidi mudar para jquery pois tenho mais experiencia.

A maior parte da minha experiencia são com sistemas monolíticos então estou aprendendo como se trabalha com API microsserviços 
